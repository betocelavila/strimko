import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DynamicValuesService {

  public size: number;
  public numberRows: number;
  public numberColumns: number;
  public game: number[][];

  constructor() {}

  public generate(): number[][] {
    this.numberRows = this.size;
    this.numberColumns = this.size;
    this.game = [];
    const rows: number[][] = this.getValues();
    const columns: number[][] = this.getValues();
    for (let row = 0; row < this.numberRows; row++) {
      this.game[row] = [];
      for (let column = 0; column < this.numberColumns; column++) {

        let count = 0;
        while (this.game[row][column] === undefined && count <= 100) {
          count++;
          const random: number = Math.floor(Math.random() * this.size) + 1;
          const rowIndex = rows[row].indexOf(random);
          const columnIndex = columns[column].indexOf(random);

          if (rowIndex !== -1 && columnIndex !== -1) {
            this.game[row][column] = random;
            rows[row][rowIndex] = undefined;
            columns[column][columnIndex] = undefined;
          }
        }
      }
    }

    if (!this.validate()) {
      this.generate();
    }
    return this.game;
  }

  public validate(): boolean {
    let valid = true;
    const sumatoria = this.sumatoria(this.size);

    for (let i = 0; i < this.numberColumns; i++) {
      let row = 0;

      for (let j = 0; j < this.numberRows; j++) {
        row = row + this.game[i][j];
      }

      if (row !== sumatoria) {
        valid = false;
      }
    }

    for (let i = 0; i < this.numberRows; i++) {
      let column = 0;

      for (let j = 0; j < this.numberColumns; j++) {
        column = column + this.game[j][i];
      }

      if (column !== sumatoria) {
        valid = false;
      }
    }
    return valid;
  }

  private getValues(): number[][] {
    const values: number[][] = [];
    for (let i = 0; i < this.size; i++) {
      values[i] = [];
      for (let j = 0; j < this.size; j++) {
        values[i][j] = j + 1;
      }
    }

    return values;
  }

  private sumatoria(x: number): number {
    return   (x * (x + 1)) / 2;
  }
}
