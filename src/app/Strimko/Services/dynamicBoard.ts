import { Injectable } from '@angular/core';
import { Boards, Node } from '../Classes/boards';

@Injectable({ providedIn: 'root' })
export class DynamicBoardService {

  grid: Node[][];
  gridFinal: Node[][];
  size: number;
  nodesOut = [];
  connectiosNode = [];
  finalConnections = new Array<Array<any>>();
  gridValue: number[][];

  constructor() {
  }

  generateByArray(grid: number[][]) {
    this.createBoardByArray(grid);
    this.nodesOut = [];
    this.finalConnections = new Array<Array<any>>();
    for (let row = 0; row < this.size; row++) {
      for (let column = 0; column < this.size; column++) {
        this.connectiosNode = [];
        if (!this.grid[row][column].inStream) {
          this.getFinalConnections(this.grid[row][column],
            this.getConnections(row, column), new Array<Node>(), this.grid[row][column]);
        }
      }
    }
    return this.finalConnections;
  }

  createBoardByArray(grid: number[][]) {
    this.grid = new Array<Array<Node>>();
    this.gridFinal = new Array<Array<Node>>();
    for (let row = 0; row < this.size; row++) {
      this.grid[row] = [];
      this.gridFinal[row] = [];
      for (let column = 0; column < this.size; column++) {
        this.grid[row][column] = {
          id: `${row}-${column}`,
          value: grid[row][column]
        };
        this.gridFinal[row][column] = {
          id: `${row}-${column}`
        };
      }
    }
  }

  getNextPosibleStep(connections: Node[]) {
    const nextStep = connections[Math.floor(Math.random() * connections.length)];
    const newConnections = connections.filter(x => x.id !== nextStep.id);
    if (connections.length === 0) {
      return undefined;
    } else if (nextStep.inStream) {
      return this.getNextPosibleStep(newConnections);
    } else {
      return nextStep;
    }
  }

  validateValues(actualNode: Node, final: Node[]): boolean {
    let result = true;
    const finalValue = final.filter(x => x.value === actualNode.value);
    if (finalValue.length > 0) { result = false; }
    return result;
  }

  getFinalConnections(actualNode: Node, connections: Node[], final: Node[], previewStep: Node) {
    const nextStep = this.getNextPosibleStep(connections);
    if (nextStep === undefined) {
      this.grid[Number(actualNode.id.split('-')[0])][Number(actualNode.id.split('-')[1])].inStream = true;
      this.nodesOut.push(actualNode.id);
      final.push(actualNode);
    }
    if (final.length === this.size) {
      this.finalConnections.push(final);
      return;
    }
    if (this.nodesOut.find(x => x === actualNode.id) === undefined) {
      if (this.validateValues(actualNode, final)) {
        this.grid[Number(actualNode.id.split('-')[0])][Number(actualNode.id.split('-')[1])].inStream = true;
        this.gridFinal[Number(actualNode.id.split('-')[0])][Number(actualNode.id.split('-')[1])].value = actualNode.value;
        this.nodesOut.push(actualNode.id);
        final.push(actualNode);
        this.getFinalConnections(nextStep,
          this.getConnections(Number(nextStep.id.split('-')[0]), Number(nextStep.id.split('-')[1])), final,
          actualNode);
      } else {
        const newPossibleConnections = this.getNextPosibleStep(
          this.getConnections(Number(previewStep.id.split('-')[0]), Number(previewStep.id.split('-')[1])));
        this.getFinalConnections(newPossibleConnections,
          this.getConnections(Number(newPossibleConnections.id.split('-')[0]), Number(newPossibleConnections.id.split('-')[1])),
          final, previewStep);
      }
    } else {
      this.getFinalConnections(this.getNextPosibleStep(connections), connections, final, actualNode);
    }
  }

  getConnections(row: number, column: number) {
    const connections = new Array<Node>();
    if (this.grid[row - 1] !== undefined && this.grid[row - 1][column - 1] !== undefined
      && !this.grid[row - 1][column - 1].inStream) {
      connections.push(this.grid[row - 1][column - 1]);
    }
    if (this.grid[row] !== undefined && this.grid[row][column - 1] !== undefined
      && !this.grid[row][column - 1].inStream) {
      connections.push(this.grid[row][column - 1]);
    }
    if (this.grid[row + 1] !== undefined && this.grid[row + 1][column - 1] !== undefined
      && !this.grid[row + 1][column - 1].inStream) {
      connections.push(this.grid[row + 1][column - 1]);
    }
    if (this.grid[row - 1] !== undefined && this.grid[row - 1][column] !== undefined
      && !this.grid[row - 1][column].inStream) {
      connections.push(this.grid[row - 1][column]);
    }
    if (this.grid[row + 1] !== undefined && this.grid[row + 1][column] !== undefined
      && !this.grid[row + 1][column].inStream) {
      connections.push(this.grid[row + 1][column]);
    }
    if (this.grid[row - 1] !== undefined && this.grid[row - 1][column + 1] !== undefined
      && !this.grid[row - 1][column + 1].inStream) {
      connections.push(this.grid[row - 1][column + 1]);
    }
    if (this.grid[row] !== undefined && this.grid[row][column + 1] !== undefined
      && !this.grid[row][column + 1].inStream) {
      connections.push(this.grid[row][column + 1]);
    }
    if (this.grid[row + 1] !== undefined && this.grid[row + 1][column + 1] !== undefined
      && !this.grid[row + 1][column + 1].inStream) {
      connections.push(this.grid[row + 1][column + 1]);
    }
    return connections;
  }

}
