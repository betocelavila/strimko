import { RouterModule, Routes } from '@angular/router';
import { DynamicComponent } from './Components/dynamic/dynamic.component';
import { StrimkoComponent } from './Components/Strimko/strimko.component';

export const routes: Routes = [
  {
    path: '',
    component: StrimkoComponent,
  },
  {
    path: 'Dynamic',
    component: DynamicComponent
  }
];


export const StrimkoRoutingModule = RouterModule.forChild(routes);
