import { Component, AfterViewInit } from '@angular/core';
import { Boards, Node } from '../../Classes/boards';
import { DynamicBoardService } from '../../Services/dynamicBoard';
import { DynamicValuesService } from '../../Services/dynamicValues';
declare var LeaderLine: any;

@Component({
  selector: 'app-strimko',
  templateUrl: './strimko.component.html',
  styleUrls: ['./strimko.component.css']
})
export class StrimkoComponent implements AfterViewInit {

  boards: Boards = new Boards();
  streams: Array<Array<Node>> = new Array<any>();
  leaderLines = [];
  size: number;
  showSolutionClicked = false;
  isLoading = false;
  valuesGame: number[][];

  constructor(private boardService: DynamicBoardService, private valueService: DynamicValuesService) {
  }

  ngAfterViewInit(): void {
  }

  newGame(item: string) {
    this.leaderLines.forEach(x => { x.remove(); });
    this.streams = [];
    this.leaderLines = [];
    this.boardService.size = Number(item.split('X')[0]);
    this.valueService.size = Number(item.split('X')[0]);
    this.valuesGame = this.valueService.generate();
    try {
      const result = this.boardService.generateByArray(this.valuesGame);
      if (result.length > this.boardService.size) {
        console.log('error.length');
        this.newGame(item);
      } else {
        this.streams = [...result];
        setTimeout(() => {
          this.paintChains();
        }, 100);
        setTimeout(() => {
          this.setValue();
        }, 200);
      }
    } catch (error) {
      console.log(error);
      this.newGame(item);
    }
  }

  setValue() {
    for (let index = 0; index < (this.streams.length - 1); index++) {
      const leftID = Math.floor(Math.random() * this.streams.length);
      const rigthID = Math.floor(Math.random() * this.streams.length);
      const el = document.getElementById(`${leftID}-${rigthID}`);
      const value = this.getElementValue(`${leftID}-${rigthID}`);
      if (value) {
        (el as any).value = value.value;
        (el as any).disabled = true;
      }
    }
  }

  getElementValue(id: string) {
    let objectReturn: any;
    this.streams.forEach(stream => {
      const object = (stream as Array<any>).find(x => x.id === id);
      if (object) {
        objectReturn = object;
      }
    });
    return objectReturn;
  }

  paintChains() {
    this.leaderLines = [];
    this.streams.forEach(stream => {
      for (let index = 0; index < stream.length; index++) {
        if ((index + 1) === stream.length) {
          return;
        } else {
          this.leaderLines.push(this.createLeaderLine(stream[index].id, stream[index + 1].id));
        }
      }
    });
  }

  createLeaderLine(id, id2) {
    const startEl = document.getElementById(id);
    const endEl = document.getElementById(id2);
    return new LeaderLine(
      startEl,
      endEl,
      {
        endPlugOutline: false,
        path: 'fluid',
        startPlug: 'behind',
        endPlug: 'behind',
        dash: { len: 16, gap: 16 },
        // startSocket: 'right',
        // endSocket: 'left',
        // startSocketGravity: 100,
        size: 1,
        animOptions: { duration: 3000, timing: 'linear' }
      }
    );
  }

  keyPress(event: any) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    } else {
      this.validateGame(event.target.id.split('-')[0], event.target.id.split('-')[1], Number(inputChar));
    }
  }

  validateGame(row: number, column: number, input: number) {
    const element = document.getElementById(`${row}-${column}`);
    if (this.valuesGame[row][column] !== input) {
      element.classList.add('has-error');
    } else {
      element.classList.remove('has-error');
    }
  }

  showResult() {
    for (let row = 0; row < this.streams.length; row++) {
      for (let column = 0; column < this.streams.length; column++) {
        const el = document.getElementById(`${row}-${column}`);
        const value = this.getElementValue(`${row}-${column}`);
        (el as any).value = value.value;
      }
    }
    this.showSolutionClicked = !this.showSolutionClicked;
  }

  hideResult() {
    for (let row = 0; row < this.streams.length; row++) {
      for (let column = 0; column < this.streams.length; column++) {
        const el = document.getElementById(`${row}-${column}`);
        if (!(el as any).disabled) {
          (el as any).value = '';
        }
      }
    }
    this.showSolutionClicked = !this.showSolutionClicked;
  }

}
