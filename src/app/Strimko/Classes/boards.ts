export class Boards  {
  streams =
    { '3X3':
      [
        [
          [{id: '0-1', value: 3}, {id: '0-0', value: 2}, {id: '1-0', value: 1}],
          [{id: '0-2', value: 1}, {id: '1-1', value: 3}, {id: '2-0', value: 2}],
          [{id: '1-2', value: 2}, {id: '2-2', value: 1}, {id: '2-1', value: 3}]
        ],
      ]
    };

}

export interface Node {
  id?: string;
  value?: number;
  inStream?: boolean;
}
