import { NgModule } from '@angular/core';
import { StrimkoComponent } from './Components/Strimko/strimko.component';
import { SharedModule } from '../Shared/shared.module';
import { StrimkoRoutingModule } from './strimko-routing.module';
import { DynamicComponent } from './Components/dynamic/dynamic.component';
import { DynamicBoardService } from './Services/dynamicBoard';

@NgModule({
    imports: [
      StrimkoRoutingModule,
      SharedModule
    ],
    declarations: [
      StrimkoComponent,
      DynamicComponent
    ],
    exports: [
      StrimkoComponent
    ],
    providers: [
      DynamicBoardService
    ]
})
export class StrimkoModule { }
