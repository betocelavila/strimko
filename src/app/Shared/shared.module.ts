import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {
  MatIconModule,
  MatMenuModule, MatGridListModule,
  MatCardModule, MatInputModule,
  MatButtonModule, MatProgressSpinnerModule
} from '@angular/material';

@NgModule({
    imports: [
      RouterModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      MatProgressSpinnerModule,
      MatInputModule,
      MatButtonModule,
      MatCardModule,
      MatGridListModule,
      MatMenuModule,
      MatIconModule,
    ],
    declarations: [
    ],
    exports: [
      RouterModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      MatProgressSpinnerModule,
      MatInputModule,
      MatButtonModule,
      MatCardModule,
      MatGridListModule,
      MatMenuModule,
      MatIconModule,
    ],
    providers: [
    ]
})
export class SharedModule { }
